﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AssetManager
{
    
    public partial class searchFailed : Form
        
    {
        homepage homepage;
        public searchFailed(homepage newHomepage)
        {
            InitializeComponent();
            homepage = newHomepage;
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            
            homepage.assetExists = false;
            homepage.btn_add_Click(sender, e);
            homepage.prepopulateNewAsset();
            this.Hide();
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
