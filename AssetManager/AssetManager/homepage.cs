﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace AssetManager
{
    public partial class homepage : Form
    {

        String returnString;
        String conn = BSModule.db_conx(Properties.Settings.Default.connectionString);
        
        public static Boolean showAllShown = false;
        public static Boolean assetExists = false;
        public static Boolean deleteChecker = false;
        int addHeight = 530;
        int addWidth = 525;
        int startHeight = 405;
        int startWidth = 105;


        searchFailed searchFailed;

       

        public homepage()
        {
            InitializeComponent();
            loadUp();

            this.Closed += new EventHandler(homepage_Closed);




        }

        private void loadUp()
        {
            this.Size = new Size(startHeight, startWidth);
            DataSet DS = BSModule.getDataSetFromPTLSAGE("assets_getUsers", conn);
            if (DS.Tables[0].Rows.Count > 0)
            {
                //populating combo box with usernames
                combo_owner.DataSource = DS.Tables[0];
                combo_owner.ValueMember = "Username";
                combo_owner.DisplayMember = "Username";
                combo_owner.SelectedIndex = -1;
            }

            autoCompleter();

        }

        public void btn_add_Click(object sender, EventArgs e)
        {
            btn_update.Text = "Add Asset";
            unlockFields();
            clearFields();
            btn_unlock.Visible = false;
            btn_update.Visible = true;
            btn_multi.Visible = true;
            this.Size = new Size(addHeight, addWidth);
            panel_detail.Visible = true;
        }


        //ensures enough data on an asset is filled in.
        private void validate()
        {
            returnString = "";
            if (txt_type.Text.Trim() == "")
            {
                returnString += "Type is missing.\n";
            }
            if (txt_make.Text.Trim() == "")
            {
                returnString += "Make is missing.\n";
            }
            if (txt_location.Text.Trim() == "")
            {
                returnString += "Location is missing.\n";
            }
            

        }

        private void btn_update_Click(object sender, EventArgs e)
        {
            String assetNo = txt_assetno.Text;
            DataSet DS = BSModule.getDataSetFromPTLSAGE("assets_searchAssets", conn, "@Barcode", assetNo);
            Boolean assetExists2 = false;
            validate();
            if (returnString != "")
            {
                MessageBox.Show(returnString);
                return;
            }
            if (DS.Tables[0].Rows.Count != 0)
            {
                assetExists2 = true;
            }
            if (assetExists2 == true)
            {
                updateAsset();
                btn_update.Visible = false;
                btn_delete.Visible = false;
                btn_unlock.Text = "Unlock";
                lockFields();
                MessageBox.Show("Asset updated successfully.");
                this.Size = new Size(startHeight, startWidth);
                clearFields();
            }
            else
            {
                addAssetCheck();
                
            }
            
            
            panel_detail.Visible = false;
            this.Size = new Size(startHeight, startWidth);
            
        }


        //function which checks to see if an asset with the desired asset tag number already exists.
        private void addAssetCheck()
        {
            String assetNo = txt_assetno.Text;
            assetNo = assetNo.PadLeft(5, '0');
            DataSet DS = BSModule.getDataSetFromPTLSAGE("assets_searchAssets", conn, "@Barcode", assetNo);
            if (DS.Tables[0].Rows.Count == 0)
            {
                if (panel_computer.Visible == true)
                {
                    if (chkbox_vga.Checked == false && chkbox_hdmi.Checked == false && chkbox_dvi.Checked == false && chkbox_dp.Checked == false)
                    {
                        MessageBox.Show("Error: Further Details Needed.");
                        return;
                    }
                }
                addAsset();
                clearFields();
                MessageBox.Show("Asset Entered.");
                

            }
            else
            {
                MessageBox.Show("Asset already exists!");
            }
        }


        //function for updating the details on an asset of which already exists.
        private void updateAsset()
        {
            string barcode = txt_assetno.Text;
            barcode = barcode.PadLeft(5, '0');

            string type = txt_type.Text.Trim();
            string make = txt_make.Text.Trim();
            string model = txt_model.Text.Trim();
            string invoiceNo = txt_invoice.Text.Trim();
            string purchaseDate = date_purchase.Value.Date.ToString("yyyy-MM-dd");
            string serialNo = txt_serial.Text.Trim();
            string owner = combo_owner.Text.Trim();
            string ipAddress = txt_ipAddress.Text.Trim();
            bool inUse = false;
            string location = txt_location.Text.Trim();
            string phoneNo = txt_phone.Text.Trim();
            double? price = null; 
            if (txt_price.Text.Trim() != "")
            {
                price = double.Parse(txt_price.Text.Trim()); 
            }
            else
            {
                price = null;
            }
      
            string cpu = txt_cpu.Text.Trim();
            int? ram = null;
            if (txt_ram.Text.Trim() != "")
            {
                ram = int.Parse(txt_ram.Text.Trim());
            }
            else
            {
                ram = null;
            }

            string displayPorts = "";

            if (chkbox_vga.Checked == true)
            {
                displayPorts = displayPorts + "VGA, ";
            }
            if (chkbox_hdmi.Checked == true)
            {
                displayPorts = displayPorts + "HDMI, ";
            }
            if (chkbox_dvi.Checked == true)
            {
                displayPorts = displayPorts + "DVI, ";
            }
            if (chkbox_dp.Checked == true)
            {
                displayPorts = displayPorts + "DP, ";
            }

            if (displayPorts != "")
            {
                displayPorts = displayPorts.Substring(0, displayPorts.LastIndexOf(','));
            }


            if (chkbox_use.Checked == true)
            {
                inUse = true;
            }


            DataSet DS = BSModule.getDataSetFromPTLSAGE("assets_updateAsset", conn, "@Barcode", barcode, "@Type", type,
                "@Make", make, "@Model", model, "@InvoiceNo", invoiceNo, "@PurchaseDate", purchaseDate.ToString(), "@SerialNo", serialNo,
                "@Owner", owner, "@InUse", inUse.ToString(), "@Location", location, "@Price", price.ToString(), "@CPU", cpu, "@RAM", ram.ToString(), "@DisplayPorts", displayPorts, "@IPAddress", ipAddress, "@phone", phoneNo);

            DataSet DS2 = BSModule.getDataSetFromPTLSAGE("assets_populateAutoComplete", conn, "@Type", type, "@Make", make, "@Model", model, "@Location", location);
        }


        //function for adding assets into the database
        private void addAsset()
        {
            string barcode = txt_assetno.Text;
            barcode = barcode.PadLeft(5, '0');

            string type = txt_type.Text.Trim();
            string make = txt_make.Text.Trim();
            string model = txt_model.Text.Trim();
            string invoiceNo = txt_invoice.Text.Trim();
            string ipAddress = txt_ipAddress.Text.Trim();

            string purchaseDate = "";
            if (date_purchase.Checked == true)
            {
                purchaseDate = date_purchase.Value.Date.ToString("yyyy-MM-dd");
            }
             
            string serialNo = txt_serial.Text.Trim();
            string owner = combo_owner.Text.Trim();
            bool inUse = false;
            string location = txt_location.Text.Trim();
            string phoneNo = txt_phone.Text.ToString();
            double? price = null; 
            if (txt_price.Text.Trim() != "")
            {
                price = double.Parse(txt_price.Text.Trim()); 
            }
            else
            {
                price = null;
            }
            
            string cpu = txt_cpu.Text.Trim();
            int? ram = null;
            if (txt_ram.Text.Trim() != "")
            {
                ram = int.Parse(txt_ram.Text.Trim());
            }
            else
            {
                ram = null;
            }
            string displayPorts = "";

            if (chkbox_vga.Checked == true)
            {
                displayPorts = displayPorts + "VGA, ";
            }
            if (chkbox_hdmi.Checked == true)
            {
                displayPorts = displayPorts + "HDMI, ";
            }
            if (chkbox_dvi.Checked == true)
            {
                displayPorts = displayPorts + "DVI, ";
            }
            if (chkbox_dp.Checked == true)
            {
                displayPorts = displayPorts + "DP, ";
            }

            if (displayPorts != "")
            {
                displayPorts = displayPorts.Substring(0, displayPorts.LastIndexOf(','));
            }

            if (chkbox_use.Checked == true)
            {
                inUse = true;
            }

         
            DataSet DS = BSModule.getDataSetFromPTLSAGE("assets_addAsset", conn, "@Barcode", barcode, "@Type", type,
                "@Make", make, "@Model", model, "@InvoiceNo", invoiceNo, "@PurchaseDate", purchaseDate.ToString(), "@SerialNo", serialNo, 
                "@Owner", owner, "@InUse", inUse.ToString(), "@Location", location, "@Price", price.ToString(), "@CPU", cpu, "@RAM", ram.ToString(), "@DisplayPorts", displayPorts, "@IPAddress", ipAddress, "@phone", phoneNo);
        
        DataSet DS2 = BSModule.getDataSetFromPTLSAGE("assets_populateAutoComplete", conn, "@Type", type, "@Make", make, "@Model", model, "@Location", location);
        }


        //Function that gives suggestions to autofill information on assets - could be extended to more fields if wanted
        private void autoCompleter()
        {
            DataSet TYPESDS = BSModule.getDataSetFromPTLSAGE("assets_autoCompleter", conn, "@Procedure", "GETTYPE");
            DataSet MAKESDS = BSModule.getDataSetFromPTLSAGE("assets_autoCompleter", conn, "@Procedure", "GETMAKE");
            DataSet MODELSDS = BSModule.getDataSetFromPTLSAGE("assets_autoCompleter", conn, "@Procedure", "GETMODEL");
            DataSet LOCATIONSDS = BSModule.getDataSetFromPTLSAGE("assets_autoCompleter", conn, "@Procedure", "GETLOCATION");

            AutoCompleteStringCollection colType = new AutoCompleteStringCollection();
            AutoCompleteStringCollection colMake = new AutoCompleteStringCollection();
            AutoCompleteStringCollection colModel = new AutoCompleteStringCollection();
            AutoCompleteStringCollection colLocation = new AutoCompleteStringCollection();

                foreach (DataRow row in TYPESDS.Tables[0].Rows)
                {
                    colType.Add(row["Type"].ToString().ToUpper());
                }
                txt_type.AutoCompleteSource = AutoCompleteSource.CustomSource;
                txt_type.AutoCompleteMode = AutoCompleteMode.Suggest;
                txt_type.AutoCompleteCustomSource = colType;

                foreach (DataRow row in MAKESDS.Tables[0].Rows)
                {
                    colMake.Add(row["Make"].ToString().ToUpper());
                }
                txt_make.AutoCompleteSource = AutoCompleteSource.CustomSource;
                txt_make.AutoCompleteMode = AutoCompleteMode.Suggest;
                txt_make.AutoCompleteCustomSource = colMake;

                foreach (DataRow row in MODELSDS.Tables[0].Rows)
                {
                    colModel.Add(row["Model"].ToString().ToUpper());
                }
                txt_model.AutoCompleteSource = AutoCompleteSource.CustomSource;
                txt_model.AutoCompleteMode = AutoCompleteMode.Suggest;
                txt_model.AutoCompleteCustomSource = colModel;

                foreach (DataRow row in LOCATIONSDS.Tables[0].Rows)
                {
                    colLocation.Add(row["Location"].ToString().ToUpper());
                }
                txt_location.AutoCompleteSource = AutoCompleteSource.CustomSource;
                txt_location.AutoCompleteMode = AutoCompleteMode.Suggest;
                txt_location.AutoCompleteCustomSource = colLocation;

            
            

        }

       
        private void txt_type_Leave(object sender, EventArgs e)
        {
            if (txt_type.Text.Trim() == "COMPUTER" || txt_type.Text.Trim() == "LAPTOP" || txt_type.Text.Trim() == "PC" || txt_type.Text.Trim() == "MONITOR")
            {
                panel_computer.Visible = true;
            }
            else
            {
                panel_computer.Visible = false;
            }
        }



        private void txt_price_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        private void txt_assetno_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void txt_ram_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }


        private void btn_showall_Click(object sender, EventArgs e)
        {
            if (showAllShown == false)
            {
            ViewAllAssets showAll = new ViewAllAssets();
            showAll.Show();
            showAllShown = true;
            this.Hide();
            }
            else
            {
                MessageBox.Show("Window already open.");
            }
        }

        private void btn_search_Click(object sender, EventArgs e)
        {
            btn_update.Text = "Update Asset";
            String assetNo = txt_search.Text.Trim();
            

            lockFields();
            btn_multi.Visible = false;
            btn_update.Visible = false;
            btn_unlock.Visible = true;

            if (assetNo != "")
            {
                assetNo = assetNo.PadLeft(5, '0');
                searchAsset(assetNo);
                panel_detail.Visible = true;
                btn_unlock.Visible = true;
            }
            else
            {
                MessageBox.Show("No Asset entered.");
            }
        }

        //function which searches database for an asset by number.
        public void searchAsset(String assetNo)
        {
            
            DataSet DS = BSModule.getDataSetFromPTLSAGE("assets_searchAssets", conn, "@Barcode", assetNo);

            if (DS.Tables[0].Rows.Count == 0)
            {
                searchFailed = new searchFailed(this);
                searchFailed.Show();
                string asset = txt_search.Text.ToString().Trim();
                asset = asset.PadLeft(5, '0');
                txt_assetno.Text = asset;
                panel_detail.Hide();
            }
            else {
                assetExists = true;
                this.Size = new Size(addHeight, addWidth);
                txt_assetno.Text = DS.Tables[0].Rows[0]["Barcode"].ToString();
                txt_type.Text = DS.Tables[0].Rows[0]["Type"].ToString();
                if (txt_type.Text.Trim() == "COMPUTER" || txt_type.Text.Trim() == "LAPTOP" || txt_type.Text.Trim() == "PC" || txt_type.Text.Trim() == "MONITOR")
                {
                    panel_computer.Visible = true;
                }
                txt_make.Text = DS.Tables[0].Rows[0]["Make"].ToString();
                txt_model.Text = DS.Tables[0].Rows[0]["Model"].ToString();
                txt_invoice.Text = DS.Tables[0].Rows[0]["InvoiceNo"].ToString();
                date_purchase.Text = DS.Tables[0].Rows[0]["PurchaseDate"].ToString();
                txt_serial.Text = DS.Tables[0].Rows[0]["SerialNo"].ToString();
                txt_phone.Text = DS.Tables[0].Rows[0]["PhoneNo"].ToString();
                combo_owner.SelectedValue = DS.Tables[0].Rows[0]["Owner"].ToString().Trim();
                if (DS.Tables[0].Rows[0]["InUse"].ToString() == "True")
                {
                    chkbox_use.Checked = true;
                }

                txt_location.Text = DS.Tables[0].Rows[0]["Location"].ToString();
                txt_price.Text = DS.Tables[0].Rows[0]["Price"].ToString();
                txt_cpu.Text = DS.Tables[0].Rows[0]["CPU"].ToString();
                txt_ram.Text = DS.Tables[0].Rows[0]["RAM"].ToString();

                if (DS.Tables[0].Rows[0]["DisplayPorts"].ToString().Contains("VGA"))
                {
                    chkbox_vga.Checked = true;
                }
                if (DS.Tables[0].Rows[0]["DisplayPorts"].ToString().Contains("HDMI"))
                {
                    chkbox_hdmi.Checked = true;
                }
                if (DS.Tables[0].Rows[0]["DisplayPorts"].ToString().Contains("DVI"))
                {
                    chkbox_dvi.Checked = true;
                }
                if (DS.Tables[0].Rows[0]["DisplayPorts"].ToString().Contains("DP"))
                {
                    chkbox_dp.Checked = true;
                }
            }

        }

        public void showAllSearchAsset(String assetNo)
        {
            btn_update.Text = "Update Asset";

            txt_assetno.Visible = true;

            lockFields();
            btn_multi.Visible = false;
            btn_update.Visible = false;
            btn_unlock.Visible = true;

            if (assetNo != "")
            {
                assetNo = assetNo.PadLeft(5, '0');
                searchAsset(assetNo);
                panel_detail.Visible = true;
                btn_unlock.Visible = true;
            }

            DataSet DS = BSModule.getDataSetFromPTLSAGE("assets_searchAssets", conn, "@Barcode", assetNo);


                assetExists = true;
                this.Size = new Size(addHeight, addWidth);
                txt_assetno.Text = DS.Tables[0].Rows[0]["Barcode"].ToString();
                txt_type.Text = DS.Tables[0].Rows[0]["Type"].ToString();
                if (txt_type.Text.Trim() == "COMPUTER" || txt_type.Text.Trim() == "LAPTOP" || txt_type.Text.Trim() == "PC" || txt_type.Text.Trim() == "MONITOR")
                {
                    panel_computer.Visible = true;
                }
                txt_make.Text = DS.Tables[0].Rows[0]["Make"].ToString();
                txt_model.Text = DS.Tables[0].Rows[0]["Model"].ToString();
                txt_invoice.Text = DS.Tables[0].Rows[0]["InvoiceNo"].ToString();
                date_purchase.Text = DS.Tables[0].Rows[0]["PurchaseDate"].ToString();
                txt_serial.Text = DS.Tables[0].Rows[0]["SerialNo"].ToString();
                txt_phone.Text = DS.Tables[0].Rows[0]["PhoneNo"].ToString();
                combo_owner.SelectedValue = DS.Tables[0].Rows[0]["Owner"].ToString().Trim();
                if (DS.Tables[0].Rows[0]["InUse"].ToString() == "True")
                {
                    chkbox_use.Checked = true;
                }

                txt_location.Text = DS.Tables[0].Rows[0]["Location"].ToString();
                txt_price.Text = DS.Tables[0].Rows[0]["Price"].ToString();
                txt_ipAddress.Text = DS.Tables[0].Rows[0]["IPAddress"].ToString();
                txt_cpu.Text = DS.Tables[0].Rows[0]["CPU"].ToString();
                txt_ram.Text = DS.Tables[0].Rows[0]["RAM"].ToString();

                if (DS.Tables[0].Rows[0]["DisplayPorts"].ToString().Contains("VGA"))
                {
                    chkbox_vga.Checked = true;
                }
                if (DS.Tables[0].Rows[0]["DisplayPorts"].ToString().Contains("HDMI"))
                {
                    chkbox_hdmi.Checked = true;
                }
                if (DS.Tables[0].Rows[0]["DisplayPorts"].ToString().Contains("DVI"))
                {
                    chkbox_dvi.Checked = true;
                }
                if (DS.Tables[0].Rows[0]["DisplayPorts"].ToString().Contains("DP"))
                {
                    chkbox_dp.Checked = true;
                }

        }

        private void txt_search_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != (char)13))
            {
                e.Handled = true;
            }
            if (e.KeyChar == (char)13)
            {
                btn_search.PerformClick();
            }
        }

        private void txt_search_Enter(object sender, EventArgs e)
        {
            txt_search.Clear();
        }

        //used to auto fill the asset number text box
        public void prepopulateNewAsset()
        {
            this.Size = new Size(addHeight, addWidth);
            panel_detail.Show();
            String autoFill = txt_search.Text.Trim();
            autoFill = autoFill.PadLeft(5, '0');
            txt_assetno.Text = autoFill;
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            btn_unlock.Text = "Unlock";
            lockFields();
            clearFields();
            btn_update.Visible = false;
            btn_delete.Visible = false;
            this.Size = new Size(startHeight, startWidth);
            panel_detail.Hide();
        }

        //clears all text fields etc
        private void clearFields()
        {
            txt_assetno.Text = "";
            txt_type.Text = "";
            txt_make.Text = "";
            txt_model.Text = "";
            txt_invoice.Text = "";
            date_purchase.ResetText();
            txt_serial.Text = "";
            combo_owner.SelectedIndex = -1;
            chkbox_use.Checked = false;
            txt_location.Text = "";
            txt_price.Text = "";
            txt_cpu.Text = "";
            txt_ram.Text = "";
            txt_ipAddress.Text = "";
            txt_phone.Text = "";
            chkbox_vga.Checked = false;
            chkbox_hdmi.Checked = false;
            chkbox_dvi.Checked = false;
            chkbox_dp.Checked = false;

        }

        private void clearFieldsMulti()
        {
            txt_serial.Text = "";
            combo_owner.SelectedIndex = -1;

        }

        public void btn_delete_Click(object sender, EventArgs e)
        {
            if (deleteChecker == false)
            {
                deleterCheck check = new deleterCheck(this);
                check.Show();
            }
            else
            {
                String assetNo = txt_assetno.Text.Trim();
                if (assetNo != "")
                {
                    searchAsset(assetNo);
                    if (assetExists == true)
                    {
                        MessageBox.Show("Asset Deleted.");
                        deleteAsset(assetNo);
                    }
                }
            }
            deleteChecker = false;
        }

        //makes the asset invisible within the application (does not remove from database)
        private void deleteAsset(String assetNo)
        {
            DataSet DS = BSModule.getDataSetFromPTLSAGE("assets_delete", conn, "@Barcode", assetNo, "@Deleted", "1");
            clearFields();
            panel_detail.Hide();
            this.Size = new Size(startHeight, startWidth);

        }

        //funtion for adding multiple assets with similiar details
        private void addMultiple()
        {
            validate();
            if (returnString != "")
            {
                MessageBox.Show(returnString);
                return;
            }

                addMultiAssetCheck();

                int i = Int32.Parse(txt_assetno.Text) + 1;
                string j = i.ToString();
                txt_assetno.Text = j.PadLeft(5, '0'); ;

        }

        //function for adding multiple assets with similiar details
        private void addMultiAssetCheck()
        {
            String assetNo = txt_assetno.Text.Trim();
            DataSet DS = BSModule.getDataSetFromPTLSAGE("assets_searchAssets", conn, "@Barcode", assetNo);
            if (DS.Tables[0].Rows.Count == 0)
            {
                addAsset();
                MessageBox.Show("Asset Entered.");
                clearFieldsMulti();
            }
            else
            {
                MessageBox.Show("Asset already exists!");
            }
        }

        private void btn_multi_Click(object sender, EventArgs e)
        {
            addMultiple();
        }

        
        //unlocks the text boxes for updating assets
        private void unlockFields()
        {
            txt_assetno.ReadOnly = false;
            txt_type.ReadOnly = false;
            txt_make.ReadOnly = false;
            txt_model.ReadOnly = false;
            txt_invoice.ReadOnly = false;
            txt_serial.ReadOnly = false;
            txt_location.ReadOnly = false;
            txt_ipAddress.ReadOnly = false;
            txt_price.ReadOnly = false;
            txt_cpu.ReadOnly = false;
            txt_ram.ReadOnly = false;
            txt_phone.ReadOnly = false;
            date_purchase.Enabled = true;

            combo_owner.Enabled = true;

            chkbox_use.AutoCheck = true;
            chkbox_vga.AutoCheck = true;
            chkbox_hdmi.AutoCheck = true;
            chkbox_dvi.AutoCheck = true;
            chkbox_dp.AutoCheck = true;
        }

        //locks the asset fields down
        private void lockFields()
        {
            btn_update.Visible = false;
            btn_multi.Visible = false;
            btn_delete.Visible = false;

            btn_unlock.Text = "Unlock";

            txt_assetno.ReadOnly = true;
            txt_type.ReadOnly = true;
            txt_make.ReadOnly = true;
            txt_model.ReadOnly = true;
            txt_invoice.ReadOnly = true;
            txt_serial.ReadOnly = true;
            txt_location.ReadOnly = true;
            txt_price.ReadOnly = true;
            txt_ipAddress.ReadOnly = true;
            txt_cpu.ReadOnly = true;
            txt_ram.ReadOnly = true;
            txt_phone.ReadOnly = true;
            date_purchase.Enabled = false;

            combo_owner.Enabled = false;

            chkbox_use.AutoCheck = false;
            chkbox_vga.AutoCheck = false;
            chkbox_hdmi.AutoCheck = false;
            chkbox_dvi.AutoCheck = false;
            chkbox_dp.AutoCheck = false;
        }

        private void btn_unlock_Click(object sender, EventArgs e)
        {
            if (btn_unlock.Text == "Unlock")
            {
                btn_update.Visible = true;
                btn_delete.Visible = true;
                btn_unlock.Text = "Lock";
                unlockFields();
            }
            else
            {
                btn_update.Visible = false;
                btn_delete.Visible = false;
                btn_unlock.Text = "Unlock";
                lockFields();
            }
        }

        private void homepage_Closed(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }


}
