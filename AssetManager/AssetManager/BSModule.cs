﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace AssetManager
{
    public class BSModule
    {

        public static DataSet getDataSetFromPTLSAGE(string storedProcedure, 
                                                    string connectionString, 
                                                    string param1Name = "",
                                                    string param1Val = "", 
                                                    string param2Name = "",
                                                    string param2Val = "",
                                                    string param3Name = "", 
                                                    string param3Val = "", 
                                                    string param4Name = "", 
                                                    string param4Val = "",
                                                    string param5Name = "", 
                                                    string param5Val = "",
                                                    string param6Name = "", 
                                                    string param6Val = "",
                                                    string param7Name = "", 
                                                    string param7Val = "",
                                                    string param8Name = "", 
                                                    string param8Val = "",
                                                    string param9Name = "", 
                                                    string param9Val = "",
                                                    string param10Name = "", 
                                                    string param10Val = "",
                                                    string param11Name = "", 
                                                    string param11Val = "",
                                                    string param12Name = "", 
                                                    string param12Val = "",
                                                    string param13Name = "", 
                                                    string param13Val = "",
                                                    string param14Name = "", 
                                                    string param14Val = "",
                                                    string param15Name = "",
                                                    string param15Val = "",
                                                    string param16Name = "",
                                                    string param16Val = "")
        {

            DataSet tempDataset = new DataSet();
            SqlCommand command = new SqlCommand();
            SqlConnection connection = default(SqlConnection);
            command.Parameters.Clear();
            connection = new SqlConnection(connectionString);
            try
            {
                connection.Open();
                command.Connection = connection;
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = storedProcedure;
                if (!string.IsNullOrEmpty(param1Name) & !string.IsNullOrEmpty(param1Val))
                {
                    command.Parameters.AddWithValue(param1Name, param1Val);
                }
                if (!string.IsNullOrEmpty(param2Name) & !string.IsNullOrEmpty(param2Val))
                {
                    command.Parameters.AddWithValue(param2Name, param2Val);
                }
                if (!string.IsNullOrEmpty(param3Name) & !string.IsNullOrEmpty(param3Val))
                {
                    command.Parameters.AddWithValue(param3Name, param3Val);
                }
                if (!string.IsNullOrEmpty(param4Name) & !string.IsNullOrEmpty(param4Val))
                {
                    command.Parameters.AddWithValue(param4Name, param4Val);
                }
                if (!string.IsNullOrEmpty(param5Name) & !string.IsNullOrEmpty(param5Val))
                {
                    command.Parameters.AddWithValue(param5Name, param5Val);
                }
                if (!string.IsNullOrEmpty(param6Name) & !string.IsNullOrEmpty(param6Val))
                {
                    command.Parameters.AddWithValue(param6Name, param6Val);
                }
                if (!string.IsNullOrEmpty(param7Name) & !string.IsNullOrEmpty(param7Val))
                {
                    command.Parameters.AddWithValue(param7Name, param7Val);
                }
                if (!string.IsNullOrEmpty(param8Name) & !string.IsNullOrEmpty(param8Val))
                {
                    command.Parameters.AddWithValue(param8Name, param8Val);
                }
                if (!string.IsNullOrEmpty(param9Name) & !string.IsNullOrEmpty(param9Val))
                {
                    command.Parameters.AddWithValue(param9Name, param9Val);
                }
                if (!string.IsNullOrEmpty(param10Name) & !string.IsNullOrEmpty(param10Val))
                {
                    command.Parameters.AddWithValue(param10Name, param10Val);
                }
                if (!string.IsNullOrEmpty(param11Name) & !string.IsNullOrEmpty(param11Val))
                {
                    command.Parameters.AddWithValue(param11Name, param11Val);
                }
                if (!string.IsNullOrEmpty(param12Name) & !string.IsNullOrEmpty(param12Val))
                {
                    command.Parameters.AddWithValue(param12Name, param12Val);
                }
                if (!string.IsNullOrEmpty(param13Name) & !string.IsNullOrEmpty(param13Val))
                {
                    command.Parameters.AddWithValue(param13Name, param13Val);
                }
                if (!string.IsNullOrEmpty(param14Name) & !string.IsNullOrEmpty(param14Val))
                {
                    command.Parameters.AddWithValue(param14Name, param14Val);
                }
                if (!string.IsNullOrEmpty(param15Name) & !string.IsNullOrEmpty(param15Val))
                {
                    command.Parameters.AddWithValue(param15Name, param15Val);
                }
                if (!string.IsNullOrEmpty(param16Name) & !string.IsNullOrEmpty(param16Val))
                {
                    command.Parameters.AddWithValue(param16Name, param16Val);
                }
                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(tempDataset);
                connection.Close();
            }
            catch (System.Exception ex)
            {
                tempDataset.Tables.Add();
                tempDataset.Tables[0].Rows.Add();
                tempDataset.Tables[0].Columns.Add();


                tempDataset.Tables[0].Rows[0][0] = ex.ToString();
                Console.WriteLine(ex.ToString());
                connection.Close();
            }

            return tempDataset;

        }

        public static String db_conx(string hash)
        {
            string returnValue = "";
            if (hash == "034cdaf241d87a07c5e5d47261a6f2de")
            {
                returnValue = "Data Source=PTLSAGE;Initial Catalog=MPSPriceCheck;User ID=admin;Password=barry250372;";
                return returnValue;
            }
            return returnValue;
        }
    }
}
