﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AssetManager
{
    public partial class extraDetail : Form
    {
        ViewAllAssets ViewAllAssets;
        String conn = BSModule.db_conx(Properties.Settings.Default.connectionString);
        public extraDetail(ViewAllAssets nViewAllAssets)
        {
            InitializeComponent();
            loadUp();
            ViewAllAssets = nViewAllAssets;
            getDetails();
        }

        private void loadUp()
        {
            DataSet DS = BSModule.getDataSetFromPTLSAGE("assets_getUsers", conn);
            if (DS.Tables[0].Rows.Count > 0)
            {

                combo_owner.DataSource = DS.Tables[0];
                combo_owner.ValueMember = "Username";
                combo_owner.DisplayMember = "Username";
                combo_owner.SelectedIndex = -1;
            }
            combo_owner.Enabled = false;
        }

        private void getDetails()
        {
            DataSet DS = BSModule.getDataSetFromPTLSAGE("assets_searchAssets", conn, "@Barcode", ViewAllAssets.assetDetail);

            txt_assetno.Text = DS.Tables[0].Rows[0]["Barcode"].ToString();
            txt_type.Text = DS.Tables[0].Rows[0]["Type"].ToString();
            if (txt_type.Text.Trim() == "COMPUTER" || txt_type.Text.Trim() == "LAPTOP" || txt_type.Text.Trim() == "PC" || txt_type.Text.Trim() == "MONITOR")
            {
                panel_computer.Visible = true;
            }
            txt_make.Text = DS.Tables[0].Rows[0]["Make"].ToString();
            txt_model.Text = DS.Tables[0].Rows[0]["Model"].ToString();
            txt_invoice.Text = DS.Tables[0].Rows[0]["InvoiceNo"].ToString();
            date_purchase.Text = DS.Tables[0].Rows[0]["PurchaseDate"].ToString();
            txt_serial.Text = DS.Tables[0].Rows[0]["SerialNo"].ToString();
            combo_owner.SelectedValue = DS.Tables[0].Rows[0]["Owner"].ToString().Trim();
            if (DS.Tables[0].Rows[0]["InUse"].ToString() == "True")
            {
                chkbox_use.Checked = true;
            }

            txt_location.Text = DS.Tables[0].Rows[0]["Location"].ToString();
            txt_price.Text = DS.Tables[0].Rows[0]["Price"].ToString();
            txt_cpu.Text = DS.Tables[0].Rows[0]["CPU"].ToString();
            txt_ram.Text = DS.Tables[0].Rows[0]["RAM"].ToString();

            if (DS.Tables[0].Rows[0]["DisplayPorts"].ToString().Contains("VGA"))
            {
                chkbox_vga.Checked = true;
            }
            if (DS.Tables[0].Rows[0]["DisplayPorts"].ToString().Contains("HDMI"))
            {
                chkbox_hdmi.Checked = true;
            }
            if (DS.Tables[0].Rows[0]["DisplayPorts"].ToString().Contains("DVI"))
            {
                chkbox_dvi.Checked = true;
            }
            if (DS.Tables[0].Rows[0]["DisplayPorts"].ToString().Contains("DP"))
            {
                chkbox_dp.Checked = true;
            }
        }

       

        }
    
}
