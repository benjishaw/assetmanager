﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AssetManager
{
    public partial class deleterCheck : Form
    {
        homepage homepage;
        public deleterCheck(homepage newHomepage)
        {
            InitializeComponent();
            homepage = newHomepage;
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            homepage.deleteChecker = true;
            homepage.btn_delete_Click(sender, e);            
            this.Close();
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }


    }
}
