﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AssetManager
{
    public partial class ViewAllAssets : Form
    {
        String conn = BSModule.db_conx(Properties.Settings.Default.connectionString);
        DataTable allAssetsDT = new DataTable();
        String filter = "";
        public static string assetDetail;
        Boolean loadInProgress = true;
        Boolean loaded = false;
        Boolean finance = false;
        Boolean it = false;
        String type = "";
        String make = "";
        String owner = "";
        String location = "";

        public ViewAllAssets()
        {
            InitializeComponent();
            loadUp();
            this.Closed += new EventHandler(viewAll_closed);
            loadInProgress = false;
            
        }

        private void loadUp()
        {
            showAll();

            DataSet DS = BSModule.getDataSetFromPTLSAGE("assets_getUsers", conn);
            if (DS.Tables[0].Rows.Count > 0)
            {

                combo_owner.DataSource = DS.Tables[0];
                combo_owner.ValueMember = "Username";
                combo_owner.DisplayMember = "Username";
                combo_owner.SelectedIndex = -1;
            }

            DataSet DS2 = BSModule.getDataSetFromPTLSAGE("assets_getType", conn, "@Procedure", "Type");
            if (DS.Tables[0].Rows.Count > 0)
            {

                combo_type.DataSource = DS2.Tables[0];
                combo_type.ValueMember = "Type";
                combo_type.DisplayMember = "Type";
                combo_type.SelectedIndex = -1;
            }

            DataSet DS3 = BSModule.getDataSetFromPTLSAGE("assets_getType", conn, "@Procedure", "Make");
            if (DS.Tables[0].Rows.Count > 0)
            {

                combo_make.DataSource = DS3.Tables[0];
                combo_make.ValueMember = "Make";
                combo_make.DisplayMember = "Make";
                combo_make.SelectedIndex = -1;
            }

            DataSet DS4 = BSModule.getDataSetFromPTLSAGE("assets_getType", conn, "@Procedure", "Location");
            if (DS.Tables[0].Rows.Count > 0)
            {

                combo_location.DataSource = DS4.Tables[0];
                combo_location.ValueMember = "Location";
                combo_location.DisplayMember = "Location";
                combo_location.SelectedIndex = -1;
            }
        }

    private void showAll()
    { 
        DataSet DS = BSModule.getDataSetFromPTLSAGE("assets_fullShowAll", conn);

        allAssetsDT = DS.Tables[0];

   

        dataGridView_assets.ReadOnly = true;
        dataGridView_assets.DataSource = DS.Tables[0];
 

        if (!loaded)
        {
            DataGridViewButtonColumn details_column = new DataGridViewButtonColumn();
            details_column.HeaderText = "More Info";
            details_column.Text = "Full Details";
            details_column.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            details_column.UseColumnTextForButtonValue = true;

            dataGridView_assets.Columns.Insert(dataGridView_assets.Columns.Count, details_column);
        }
        loaded = true;

        if (finance == false)
        {
            dataGridView_assets.Columns["InvoiceNo"].Visible = false;
            dataGridView_assets.Columns["PurchaseDate"].Visible = false;
            dataGridView_assets.Columns["Price"].Visible = false;
        }
        else
        {
            dataGridView_assets.Columns["InvoiceNo"].Visible = true;
            dataGridView_assets.Columns["PurchaseDate"].Visible = true;
            dataGridView_assets.Columns["Price"].Visible = true;
        }

        if (it == false)
        {
            dataGridView_assets.Columns["CPU"].Visible = false;
            dataGridView_assets.Columns["RAM"].Visible = false;
            dataGridView_assets.Columns["DisplayPorts"].Visible = false;
            dataGridView_assets.Columns["IPAddress"].Visible = false;
        }
        else
        {
            dataGridView_assets.Columns["CPU"].Visible = true;
            dataGridView_assets.Columns["RAM"].Visible = true;
            dataGridView_assets.Columns["DisplayPorts"].Visible = true;
            dataGridView_assets.Columns["IPAddress"].Visible = true;
        }



    }



    private void combo_type_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (loadInProgress == false)
        {
            if (filter.Contains("AND [Type]"))
            {
                String test = "AND [Type] = '" + type + "'";
                filter = filter.Replace(test, "");
            }
            else if (filter.Contains("Type") == true)
            {
                String test = "[Type] = '" + type + "'";
                filter = filter.Replace(test, "");
            }

            testFilter();
            type = combo_type.SelectedValue.ToString();

            if (filter == "")
            {
                filter += "[Type] = '" + type + "'";
            }
            else
            {
                filter += "AND [Type] = '" + type + "'";
            }
            DataView typeFilter = new DataView(allAssetsDT, filter, "[Barcode] ASC", DataViewRowState.CurrentRows);
            dataGridView_assets.DataSource = typeFilter;
        } 
    }

    private void combo_make_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (loadInProgress == false)
        {
            if (filter.Contains("AND [Make]"))
            {
                String test = "AND [Make] = '" + make + "'";
                filter = filter.Replace(test, "");
            }
            else if (filter.Contains("Make") == true)
            {
                String test = "[Make] = '" + make + "'";
                filter = filter.Replace(test, "");
            }

            testFilter();
            make = combo_make.SelectedValue.ToString();
            if (filter == "")
            {
                filter += "[Make] = '" + make + "'";
            }
            else
            {
                filter += "AND [Make] = '" + make + "'";
            }
            
            DataView typeFilter = new DataView(allAssetsDT, filter, "[Barcode] ASC", DataViewRowState.CurrentRows);
            dataGridView_assets.DataSource = typeFilter;
        }
    }

    private void combo_owner_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (loadInProgress == false)
        {
             if (filter.Contains("AND [Owner]"))
            {
                String test = "AND [Owner] = '" + owner + "'";
                filter = filter.Replace(test, "");
            }
            else if (filter.Contains("Owner") == true)
            {
                String test = "[Owner] = '" + owner + "'";
                filter = filter.Replace(test, "");
            }

            testFilter();
            owner = combo_owner.SelectedValue.ToString();

            if (filter == "")
            {
                filter += "[Owner] = '" + owner + "'";
            }
            else
            {
                filter += "AND [Owner] = '" + owner + "'";
            }
            DataView typeFilter = new DataView(allAssetsDT, filter, "[Barcode] ASC", DataViewRowState.CurrentRows);
            dataGridView_assets.DataSource = typeFilter;
        }
    }

    private void combo_location_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (loadInProgress == false)
        {
            if (filter.Contains("AND [Location]"))
            {
                String test = "AND [Location] = '" + location + "'";
                filter = filter.Replace(test, "");
            }
            else if (filter.Contains("Location") == true)
            {
                String test = "[Location] = '" + location + "'";
                filter = filter.Replace(test, "");
            }

            testFilter();
            location = combo_location.SelectedValue.ToString();
            
            if (filter == "")
            {
                filter += "[Location] = '" + location + "'";
            }
            else
            {
                filter += "AND [Location] = '" + location + "'";
            }
            DataView typeFilter = new DataView(allAssetsDT, filter, "[Barcode] ASC", DataViewRowState.CurrentRows);
            dataGridView_assets.DataSource = typeFilter;
        }
    }

    private void testFilter()
    {
        if (filter.StartsWith("AND"))
        {
            filter = filter.Substring(4);
        }
    }

    private void chkbox_inuse_CheckedChanged(object sender, EventArgs e)
    {

        if (filter.Contains("AND [InUse]"))
        {

            filter = filter.Replace("AND [InUse] = 'True'", "");
        }
        else if (filter.Contains("InUse") == true)
        {
            filter = filter.Replace("[InUse] = 'True'", "");
        }

        testFilter();

        if (filter == "")
        {
            filter += "[InUse] = 'True'";
        }
        else
        {
            filter += "AND [InUse] = 'True'";
        }
        
        DataView typeFilter = new DataView(allAssetsDT, filter, "[Barcode] ASC", DataViewRowState.CurrentRows);
        dataGridView_assets.DataSource = typeFilter;
    }

    private void btn_clear_Click(object sender, EventArgs e)
    {
        clearFilter();
        loadInProgress = true;
        combo_type.SelectedIndex = -1;
        combo_make.SelectedIndex = -1;
        combo_owner.SelectedIndex = -1;
        combo_location.SelectedIndex = -1;
        chkbox_inuse.Checked = false;
        showAll();
        loadInProgress = false;

    }

    private void clearFilter()
    {
        filter = "";

    }

    private void ViewAllAssets_FormClosed(object sender, FormClosedEventArgs e)
    {
        homepage.showAllShown = false;
    }

    private void btn_excel_Click(object sender, EventArgs e)
    {
        exportToExcel();
    }


    private void exportToExcel()
    {

        DataView typeFilter = new DataView(allAssetsDT, filter, "[Barcode] ASC", DataViewRowState.CurrentRows);
        DataTable dtFiltered = new DataTable();
        dtFiltered = typeFilter.ToTable();

        string dir = "u:\\Assets";
        if ((!System.IO.Directory.Exists(dir)))
        {
             System.IO.Directory.CreateDirectory(dir);
        }
        // creating Excel Application  
        Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();
        // creating new WorkBook within Excel application  
        Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add(Type.Missing);
        // creating new Excelsheet in workbook  
        Microsoft.Office.Interop.Excel._Worksheet worksheet = null;
        // see the excel sheet behind the program  
        app.Visible = true;
        // get the reference of first sheet. By default its name is Sheet1.  
        // store its reference to worksheet  
        worksheet = workbook.Sheets["Sheet1"];
        worksheet = workbook.ActiveSheet;
        // changing the name of active sheet  
        worksheet.Name = "Assets";
        // storing header part in Excel 
        int k = 1;
        foreach (DataColumn dc in dtFiltered.Columns)
        {
            worksheet.Cells[1, k] = dc.ColumnName;
            k++;
        }

        int j=1;

        foreach (DataRow dr in dtFiltered.Rows) 
        { 
            j++;  
            for (int i = 0; i < dtFiltered.Columns.Count; i++)              
            { 
                worksheet.Cells[j, i + 1] =  dr[i].ToString(); 
            }
        }
        // save the application  
        try
        {
            workbook.SaveAs(dir, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
        }
        catch (Exception)
        {
        }
            // Exit from the application  
        //app.Quit();  
    }

    private void dataGridView_assets_CellContentClick(object sender, DataGridViewCellEventArgs e)
    {
        if (dataGridView_assets.Columns[e.ColumnIndex].HeaderText == "More Info" && e.RowIndex >= 0)
        {
            assetDetail = dataGridView_assets.Rows[e.RowIndex].Cells[2].Value.ToString();
            //extraDetail showDetail = new extraDetail(this);
            //showDetail.Show();
            homepage nHomepage = new homepage();
            nHomepage.Show();
            nHomepage.showAllSearchAsset(assetDetail);
            homepage.showAllShown = false;
            this.Hide();
        }
    }


    private void viewAll_closed(object sender, EventArgs e)
    {
        homepage nHomepage = new homepage();
        nHomepage.Show();
    }

    private void chkbox_finance_CheckedChanged(object sender, EventArgs e)
    {
        if (finance == false)
        {
            finance = true;
            showAll();
        }
        else
        {
            finance = false;
            showAll();
        }
    }

    private void chkbox_it_CheckedChanged(object sender, EventArgs e)
    {
        if (it == false)
        {
            it = true;
            showAll();
        }
        else
        {
            it = false;
            showAll();
        }
    }


}
}
