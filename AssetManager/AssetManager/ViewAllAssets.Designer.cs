﻿namespace AssetManager
{
    partial class ViewAllAssets
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewAllAssets));
            this.dataGridView_assets = new System.Windows.Forms.DataGridView();
            this.combo_type = new System.Windows.Forms.ComboBox();
            this.combo_location = new System.Windows.Forms.ComboBox();
            this.combo_owner = new System.Windows.Forms.ComboBox();
            this.combo_make = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.chkbox_inuse = new System.Windows.Forms.CheckBox();
            this.btn_clear = new System.Windows.Forms.Button();
            this.btn_excel = new System.Windows.Forms.Button();
            this.chkbox_finance = new System.Windows.Forms.CheckBox();
            this.chkbox_it = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_assets)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView_assets
            // 
            this.dataGridView_assets.AllowUserToAddRows = false;
            this.dataGridView_assets.AllowUserToDeleteRows = false;
            this.dataGridView_assets.AllowUserToResizeColumns = false;
            this.dataGridView_assets.AllowUserToResizeRows = false;
            this.dataGridView_assets.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView_assets.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_assets.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridView_assets.Location = new System.Drawing.Point(2, 47);
            this.dataGridView_assets.Name = "dataGridView_assets";
            this.dataGridView_assets.ReadOnly = true;
            this.dataGridView_assets.RowHeadersVisible = false;
            this.dataGridView_assets.Size = new System.Drawing.Size(1019, 593);
            this.dataGridView_assets.TabIndex = 0;
            this.dataGridView_assets.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_assets_CellContentClick);
            // 
            // combo_type
            // 
            this.combo_type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combo_type.FormattingEnabled = true;
            this.combo_type.Location = new System.Drawing.Point(2, 20);
            this.combo_type.Name = "combo_type";
            this.combo_type.Size = new System.Drawing.Size(121, 21);
            this.combo_type.TabIndex = 1;
            this.combo_type.SelectedIndexChanged += new System.EventHandler(this.combo_type_SelectedIndexChanged);
            // 
            // combo_location
            // 
            this.combo_location.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combo_location.FormattingEnabled = true;
            this.combo_location.Location = new System.Drawing.Point(428, 20);
            this.combo_location.Name = "combo_location";
            this.combo_location.Size = new System.Drawing.Size(121, 21);
            this.combo_location.TabIndex = 2;
            this.combo_location.SelectedIndexChanged += new System.EventHandler(this.combo_location_SelectedIndexChanged);
            // 
            // combo_owner
            // 
            this.combo_owner.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combo_owner.FormattingEnabled = true;
            this.combo_owner.Location = new System.Drawing.Point(287, 20);
            this.combo_owner.Name = "combo_owner";
            this.combo_owner.Size = new System.Drawing.Size(121, 21);
            this.combo_owner.TabIndex = 3;
            this.combo_owner.SelectedIndexChanged += new System.EventHandler(this.combo_owner_SelectedIndexChanged);
            // 
            // combo_make
            // 
            this.combo_make.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combo_make.FormattingEnabled = true;
            this.combo_make.Location = new System.Drawing.Point(141, 20);
            this.combo_make.Name = "combo_make";
            this.combo_make.Size = new System.Drawing.Size(121, 21);
            this.combo_make.TabIndex = 4;
            this.combo_make.SelectedIndexChanged += new System.EventHandler(this.combo_make_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(-1, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Type";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(138, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Make";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(301, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 13);
            this.label3.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(425, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Location";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(284, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Owner";
            // 
            // chkbox_inuse
            // 
            this.chkbox_inuse.AutoSize = true;
            this.chkbox_inuse.Location = new System.Drawing.Point(570, 24);
            this.chkbox_inuse.Name = "chkbox_inuse";
            this.chkbox_inuse.Size = new System.Drawing.Size(63, 17);
            this.chkbox_inuse.TabIndex = 10;
            this.chkbox_inuse.Text = "In Use?";
            this.chkbox_inuse.UseVisualStyleBackColor = true;
            this.chkbox_inuse.CheckedChanged += new System.EventHandler(this.chkbox_inuse_CheckedChanged);
            // 
            // btn_clear
            // 
            this.btn_clear.Location = new System.Drawing.Point(639, 20);
            this.btn_clear.Name = "btn_clear";
            this.btn_clear.Size = new System.Drawing.Size(75, 23);
            this.btn_clear.TabIndex = 11;
            this.btn_clear.Text = "Clear Filter";
            this.btn_clear.UseVisualStyleBackColor = true;
            this.btn_clear.Click += new System.EventHandler(this.btn_clear_Click);
            // 
            // btn_excel
            // 
            this.btn_excel.Location = new System.Drawing.Point(738, 20);
            this.btn_excel.Name = "btn_excel";
            this.btn_excel.Size = new System.Drawing.Size(99, 23);
            this.btn_excel.TabIndex = 12;
            this.btn_excel.Text = "Export to Excel";
            this.btn_excel.UseVisualStyleBackColor = true;
            this.btn_excel.Click += new System.EventHandler(this.btn_excel_Click);
            // 
            // chkbox_finance
            // 
            this.chkbox_finance.AutoSize = true;
            this.chkbox_finance.Location = new System.Drawing.Point(876, 3);
            this.chkbox_finance.Name = "chkbox_finance";
            this.chkbox_finance.Size = new System.Drawing.Size(66, 17);
            this.chkbox_finance.TabIndex = 13;
            this.chkbox_finance.Text = "Finance";
            this.chkbox_finance.UseVisualStyleBackColor = true;
            this.chkbox_finance.CheckedChanged += new System.EventHandler(this.chkbox_finance_CheckedChanged);
            // 
            // chkbox_it
            // 
            this.chkbox_it.AutoSize = true;
            this.chkbox_it.Location = new System.Drawing.Point(876, 26);
            this.chkbox_it.Name = "chkbox_it";
            this.chkbox_it.Size = new System.Drawing.Size(34, 17);
            this.chkbox_it.TabIndex = 14;
            this.chkbox_it.Text = "IT";
            this.chkbox_it.UseVisualStyleBackColor = true;
            this.chkbox_it.CheckedChanged += new System.EventHandler(this.chkbox_it_CheckedChanged);
            // 
            // ViewAllAssets
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(1014, 641);
            this.Controls.Add(this.chkbox_it);
            this.Controls.Add(this.chkbox_finance);
            this.Controls.Add(this.btn_excel);
            this.Controls.Add(this.btn_clear);
            this.Controls.Add(this.chkbox_inuse);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.combo_make);
            this.Controls.Add(this.combo_owner);
            this.Controls.Add(this.combo_location);
            this.Controls.Add(this.combo_type);
            this.Controls.Add(this.dataGridView_assets);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ViewAllAssets";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Asset Manager";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ViewAllAssets_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_assets)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView_assets;
        private System.Windows.Forms.ComboBox combo_type;
        private System.Windows.Forms.ComboBox combo_location;
        private System.Windows.Forms.ComboBox combo_owner;
        private System.Windows.Forms.ComboBox combo_make;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox chkbox_inuse;
        private System.Windows.Forms.Button btn_clear;
        private System.Windows.Forms.Button btn_excel;
        private System.Windows.Forms.CheckBox chkbox_finance;
        private System.Windows.Forms.CheckBox chkbox_it;
    }
}