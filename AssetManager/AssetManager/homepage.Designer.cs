﻿namespace AssetManager
{
    partial class homepage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(homepage));
            this.panel_detail = new System.Windows.Forms.Panel();
            this.label20 = new System.Windows.Forms.Label();
            this.txt_ipAddress = new System.Windows.Forms.TextBox();
            this.btn_unlock = new System.Windows.Forms.Button();
            this.btn_multi = new System.Windows.Forms.Button();
            this.btn_cancel = new System.Windows.Forms.Button();
            this.date_purchase = new System.Windows.Forms.DateTimePicker();
            this.combo_owner = new System.Windows.Forms.ComboBox();
            this.chkbox_use = new System.Windows.Forms.CheckBox();
            this.btn_delete = new System.Windows.Forms.Button();
            this.btn_update = new System.Windows.Forms.Button();
            this.panel_computer = new System.Windows.Forms.Panel();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.chkbox_dp = new System.Windows.Forms.CheckBox();
            this.chkbox_vga = new System.Windows.Forms.CheckBox();
            this.chkbox_hdmi = new System.Windows.Forms.CheckBox();
            this.chkbox_dvi = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txt_cpu = new System.Windows.Forms.TextBox();
            this.txt_ram = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txt_price = new System.Windows.Forms.TextBox();
            this.txt_location = new System.Windows.Forms.TextBox();
            this.txt_serial = new System.Windows.Forms.TextBox();
            this.txt_invoice = new System.Windows.Forms.TextBox();
            this.txt_model = new System.Windows.Forms.TextBox();
            this.txt_make = new System.Windows.Forms.TextBox();
            this.txt_type = new System.Windows.Forms.TextBox();
            this.txt_assetno = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_showall = new System.Windows.Forms.Button();
            this.btn_add = new System.Windows.Forms.Button();
            this.btn_search = new System.Windows.Forms.Button();
            this.txt_search = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.txt_phone = new System.Windows.Forms.TextBox();
            this.panel_detail.SuspendLayout();
            this.panel_computer.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel_detail
            // 
            this.panel_detail.Controls.Add(this.txt_phone);
            this.panel_detail.Controls.Add(this.label21);
            this.panel_detail.Controls.Add(this.label20);
            this.panel_detail.Controls.Add(this.txt_ipAddress);
            this.panel_detail.Controls.Add(this.btn_unlock);
            this.panel_detail.Controls.Add(this.btn_multi);
            this.panel_detail.Controls.Add(this.btn_cancel);
            this.panel_detail.Controls.Add(this.date_purchase);
            this.panel_detail.Controls.Add(this.combo_owner);
            this.panel_detail.Controls.Add(this.chkbox_use);
            this.panel_detail.Controls.Add(this.btn_delete);
            this.panel_detail.Controls.Add(this.btn_update);
            this.panel_detail.Controls.Add(this.panel_computer);
            this.panel_detail.Controls.Add(this.txt_price);
            this.panel_detail.Controls.Add(this.txt_location);
            this.panel_detail.Controls.Add(this.txt_serial);
            this.panel_detail.Controls.Add(this.txt_invoice);
            this.panel_detail.Controls.Add(this.txt_model);
            this.panel_detail.Controls.Add(this.txt_make);
            this.panel_detail.Controls.Add(this.txt_type);
            this.panel_detail.Controls.Add(this.txt_assetno);
            this.panel_detail.Controls.Add(this.label14);
            this.panel_detail.Controls.Add(this.label12);
            this.panel_detail.Controls.Add(this.label11);
            this.panel_detail.Controls.Add(this.label10);
            this.panel_detail.Controls.Add(this.label8);
            this.panel_detail.Controls.Add(this.label7);
            this.panel_detail.Controls.Add(this.label6);
            this.panel_detail.Controls.Add(this.label5);
            this.panel_detail.Controls.Add(this.label4);
            this.panel_detail.Controls.Add(this.label3);
            this.panel_detail.Controls.Add(this.label2);
            this.panel_detail.Location = new System.Drawing.Point(-2, 85);
            this.panel_detail.Name = "panel_detail";
            this.panel_detail.Size = new System.Drawing.Size(514, 403);
            this.panel_detail.TabIndex = 11;
            this.panel_detail.Visible = false;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(285, 98);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(66, 13);
            this.label20.TabIndex = 39;
            this.label20.Text = "IP Address: ";
            // 
            // txt_ipAddress
            // 
            this.txt_ipAddress.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_ipAddress.Location = new System.Drawing.Point(363, 95);
            this.txt_ipAddress.Name = "txt_ipAddress";
            this.txt_ipAddress.ReadOnly = true;
            this.txt_ipAddress.Size = new System.Drawing.Size(100, 22);
            this.txt_ipAddress.TabIndex = 38;
            // 
            // btn_unlock
            // 
            this.btn_unlock.Location = new System.Drawing.Point(27, 336);
            this.btn_unlock.Name = "btn_unlock";
            this.btn_unlock.Size = new System.Drawing.Size(117, 23);
            this.btn_unlock.TabIndex = 37;
            this.btn_unlock.Text = "Unlock";
            this.btn_unlock.UseVisualStyleBackColor = true;
            this.btn_unlock.Visible = false;
            this.btn_unlock.Click += new System.EventHandler(this.btn_unlock_Click);
            // 
            // btn_multi
            // 
            this.btn_multi.Location = new System.Drawing.Point(396, 336);
            this.btn_multi.Name = "btn_multi";
            this.btn_multi.Size = new System.Drawing.Size(115, 23);
            this.btn_multi.TabIndex = 36;
            this.btn_multi.Text = "Add Multiple";
            this.btn_multi.UseVisualStyleBackColor = true;
            this.btn_multi.Visible = false;
            this.btn_multi.Click += new System.EventHandler(this.btn_multi_Click);
            // 
            // btn_cancel
            // 
            this.btn_cancel.Location = new System.Drawing.Point(396, 372);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(117, 23);
            this.btn_cancel.TabIndex = 35;
            this.btn_cancel.Text = "Cancel";
            this.btn_cancel.UseVisualStyleBackColor = true;
            this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
            // 
            // date_purchase
            // 
            this.date_purchase.Checked = false;
            this.date_purchase.Enabled = false;
            this.date_purchase.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.date_purchase.Location = new System.Drawing.Point(142, 188);
            this.date_purchase.Name = "date_purchase";
            this.date_purchase.ShowCheckBox = true;
            this.date_purchase.Size = new System.Drawing.Size(101, 22);
            this.date_purchase.TabIndex = 34;
            // 
            // combo_owner
            // 
            this.combo_owner.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combo_owner.FormattingEnabled = true;
            this.combo_owner.Location = new System.Drawing.Point(143, 250);
            this.combo_owner.Name = "combo_owner";
            this.combo_owner.Size = new System.Drawing.Size(100, 21);
            this.combo_owner.TabIndex = 33;
            // 
            // chkbox_use
            // 
            this.chkbox_use.AutoCheck = false;
            this.chkbox_use.AutoSize = true;
            this.chkbox_use.Location = new System.Drawing.Point(143, 292);
            this.chkbox_use.Name = "chkbox_use";
            this.chkbox_use.Size = new System.Drawing.Size(15, 14);
            this.chkbox_use.TabIndex = 32;
            this.chkbox_use.UseVisualStyleBackColor = true;
            // 
            // btn_delete
            // 
            this.btn_delete.Location = new System.Drawing.Point(173, 372);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Size = new System.Drawing.Size(117, 23);
            this.btn_delete.TabIndex = 31;
            this.btn_delete.Text = "Delete Asset";
            this.btn_delete.UseVisualStyleBackColor = true;
            this.btn_delete.Visible = false;
            this.btn_delete.Click += new System.EventHandler(this.btn_delete_Click);
            // 
            // btn_update
            // 
            this.btn_update.Location = new System.Drawing.Point(27, 372);
            this.btn_update.Name = "btn_update";
            this.btn_update.Size = new System.Drawing.Size(117, 23);
            this.btn_update.TabIndex = 30;
            this.btn_update.Text = "Update Asset";
            this.btn_update.UseVisualStyleBackColor = true;
            this.btn_update.Visible = false;
            this.btn_update.Click += new System.EventHandler(this.btn_update_Click);
            // 
            // panel_computer
            // 
            this.panel_computer.Controls.Add(this.label19);
            this.panel_computer.Controls.Add(this.label18);
            this.panel_computer.Controls.Add(this.label17);
            this.panel_computer.Controls.Add(this.label13);
            this.panel_computer.Controls.Add(this.chkbox_dp);
            this.panel_computer.Controls.Add(this.chkbox_vga);
            this.panel_computer.Controls.Add(this.chkbox_hdmi);
            this.panel_computer.Controls.Add(this.chkbox_dvi);
            this.panel_computer.Controls.Add(this.label9);
            this.panel_computer.Controls.Add(this.txt_cpu);
            this.panel_computer.Controls.Add(this.txt_ram);
            this.panel_computer.Controls.Add(this.label15);
            this.panel_computer.Controls.Add(this.label16);
            this.panel_computer.Location = new System.Drawing.Point(277, 160);
            this.panel_computer.Name = "panel_computer";
            this.panel_computer.Size = new System.Drawing.Size(225, 164);
            this.panel_computer.TabIndex = 29;
            this.panel_computer.Visible = false;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(196, 71);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(21, 13);
            this.label19.TabIndex = 35;
            this.label19.Text = "DP";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(127, 71);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(25, 13);
            this.label18.TabIndex = 35;
            this.label18.Text = "DVI";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(157, 71);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(36, 13);
            this.label17.TabIndex = 34;
            this.label17.Text = "HDMI";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(92, 71);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(29, 13);
            this.label13.TabIndex = 33;
            this.label13.Text = "VGA";
            // 
            // chkbox_dp
            // 
            this.chkbox_dp.AutoCheck = false;
            this.chkbox_dp.AutoSize = true;
            this.chkbox_dp.Location = new System.Drawing.Point(199, 87);
            this.chkbox_dp.Name = "chkbox_dp";
            this.chkbox_dp.Size = new System.Drawing.Size(15, 14);
            this.chkbox_dp.TabIndex = 32;
            this.chkbox_dp.UseVisualStyleBackColor = true;
            // 
            // chkbox_vga
            // 
            this.chkbox_vga.AutoCheck = false;
            this.chkbox_vga.AutoSize = true;
            this.chkbox_vga.Location = new System.Drawing.Point(95, 87);
            this.chkbox_vga.Name = "chkbox_vga";
            this.chkbox_vga.Size = new System.Drawing.Size(15, 14);
            this.chkbox_vga.TabIndex = 31;
            this.chkbox_vga.UseVisualStyleBackColor = true;
            // 
            // chkbox_hdmi
            // 
            this.chkbox_hdmi.AutoCheck = false;
            this.chkbox_hdmi.AutoSize = true;
            this.chkbox_hdmi.Location = new System.Drawing.Point(160, 87);
            this.chkbox_hdmi.Name = "chkbox_hdmi";
            this.chkbox_hdmi.Size = new System.Drawing.Size(15, 14);
            this.chkbox_hdmi.TabIndex = 30;
            this.chkbox_hdmi.UseVisualStyleBackColor = true;
            // 
            // chkbox_dvi
            // 
            this.chkbox_dvi.AutoCheck = false;
            this.chkbox_dvi.AutoSize = true;
            this.chkbox_dvi.Location = new System.Drawing.Point(126, 87);
            this.chkbox_dvi.Name = "chkbox_dvi";
            this.chkbox_dvi.Size = new System.Drawing.Size(15, 14);
            this.chkbox_dvi.TabIndex = 29;
            this.chkbox_dvi.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 9);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(34, 13);
            this.label9.TabIndex = 7;
            this.label9.Text = "CPU: ";
            // 
            // txt_cpu
            // 
            this.txt_cpu.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_cpu.Location = new System.Drawing.Point(87, 3);
            this.txt_cpu.Name = "txt_cpu";
            this.txt_cpu.ReadOnly = true;
            this.txt_cpu.Size = new System.Drawing.Size(100, 22);
            this.txt_cpu.TabIndex = 26;
            // 
            // txt_ram
            // 
            this.txt_ram.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_ram.Location = new System.Drawing.Point(87, 34);
            this.txt_ram.Name = "txt_ram";
            this.txt_ram.ReadOnly = true;
            this.txt_ram.Size = new System.Drawing.Size(100, 22);
            this.txt_ram.TabIndex = 27;
            this.txt_ram.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_ram_KeyPress);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(12, 43);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(61, 13);
            this.label15.TabIndex = 13;
            this.label15.Text = "RAM (GB): ";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(12, 88);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(73, 13);
            this.label16.TabIndex = 14;
            this.label16.Text = "Display Ports";
            // 
            // txt_price
            // 
            this.txt_price.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_price.Location = new System.Drawing.Point(363, 57);
            this.txt_price.Name = "txt_price";
            this.txt_price.ReadOnly = true;
            this.txt_price.Size = new System.Drawing.Size(100, 22);
            this.txt_price.TabIndex = 25;
            this.txt_price.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_price_KeyPress);
            // 
            // txt_location
            // 
            this.txt_location.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_location.Location = new System.Drawing.Point(363, 18);
            this.txt_location.Name = "txt_location";
            this.txt_location.ReadOnly = true;
            this.txt_location.Size = new System.Drawing.Size(100, 22);
            this.txt_location.TabIndex = 24;
            // 
            // txt_serial
            // 
            this.txt_serial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_serial.Location = new System.Drawing.Point(143, 216);
            this.txt_serial.Name = "txt_serial";
            this.txt_serial.ReadOnly = true;
            this.txt_serial.Size = new System.Drawing.Size(100, 22);
            this.txt_serial.TabIndex = 21;
            // 
            // txt_invoice
            // 
            this.txt_invoice.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_invoice.Location = new System.Drawing.Point(143, 151);
            this.txt_invoice.Name = "txt_invoice";
            this.txt_invoice.ReadOnly = true;
            this.txt_invoice.Size = new System.Drawing.Size(100, 22);
            this.txt_invoice.TabIndex = 19;
            // 
            // txt_model
            // 
            this.txt_model.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_model.Location = new System.Drawing.Point(143, 117);
            this.txt_model.Name = "txt_model";
            this.txt_model.ReadOnly = true;
            this.txt_model.Size = new System.Drawing.Size(100, 22);
            this.txt_model.TabIndex = 18;
            // 
            // txt_make
            // 
            this.txt_make.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_make.Location = new System.Drawing.Point(143, 86);
            this.txt_make.Name = "txt_make";
            this.txt_make.ReadOnly = true;
            this.txt_make.Size = new System.Drawing.Size(100, 22);
            this.txt_make.TabIndex = 17;
            // 
            // txt_type
            // 
            this.txt_type.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txt_type.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txt_type.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_type.Location = new System.Drawing.Point(143, 56);
            this.txt_type.Name = "txt_type";
            this.txt_type.ReadOnly = true;
            this.txt_type.Size = new System.Drawing.Size(100, 22);
            this.txt_type.TabIndex = 16;
            this.txt_type.Leave += new System.EventHandler(this.txt_type_Leave);
            // 
            // txt_assetno
            // 
            this.txt_assetno.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_assetno.Location = new System.Drawing.Point(143, 18);
            this.txt_assetno.MaxLength = 5;
            this.txt_assetno.Name = "txt_assetno";
            this.txt_assetno.ReadOnly = true;
            this.txt_assetno.Size = new System.Drawing.Size(100, 22);
            this.txt_assetno.TabIndex = 15;
            this.txt_assetno.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_assetno_KeyPress);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(18, 258);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(48, 13);
            this.label14.TabIndex = 12;
            this.label14.Text = "Owner: ";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(18, 293);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(50, 13);
            this.label12.TabIndex = 10;
            this.label12.Text = "In Use?: ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(285, 29);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(57, 13);
            this.label11.TabIndex = 9;
            this.label11.Text = "Location: ";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(286, 66);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(55, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "Price (£):  ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(18, 65);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "Type: ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(18, 95);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "Make: ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(18, 126);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Model: ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(18, 160);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(93, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Invoice Number: ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 194);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Purchase Date: ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 225);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Serial Number: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Asset No: ";
            // 
            // btn_showall
            // 
            this.btn_showall.Location = new System.Drawing.Point(275, 36);
            this.btn_showall.Name = "btn_showall";
            this.btn_showall.Size = new System.Drawing.Size(104, 23);
            this.btn_showall.TabIndex = 10;
            this.btn_showall.Text = "Show All Assets";
            this.btn_showall.UseVisualStyleBackColor = true;
            this.btn_showall.Click += new System.EventHandler(this.btn_showall_Click);
            // 
            // btn_add
            // 
            this.btn_add.Location = new System.Drawing.Point(274, 7);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(105, 23);
            this.btn_add.TabIndex = 9;
            this.btn_add.Text = "Add New Asset";
            this.btn_add.UseVisualStyleBackColor = true;
            this.btn_add.Click += new System.EventHandler(this.btn_add_Click);
            // 
            // btn_search
            // 
            this.btn_search.Location = new System.Drawing.Point(114, 33);
            this.btn_search.Name = "btn_search";
            this.btn_search.Size = new System.Drawing.Size(75, 23);
            this.btn_search.TabIndex = 8;
            this.btn_search.Text = "Search";
            this.btn_search.UseVisualStyleBackColor = true;
            this.btn_search.Click += new System.EventHandler(this.btn_search_Click);
            // 
            // txt_search
            // 
            this.txt_search.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_search.Location = new System.Drawing.Point(7, 33);
            this.txt_search.MaxLength = 5;
            this.txt_search.Name = "txt_search";
            this.txt_search.Size = new System.Drawing.Size(100, 22);
            this.txt_search.TabIndex = 7;
            this.txt_search.Enter += new System.EventHandler(this.txt_search_Enter);
            this.txt_search.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_search_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(2, -1);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(230, 30);
            this.label1.TabIndex = 6;
            this.label1.Text = "Search Asset Number:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(288, 126);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(58, 13);
            this.label21.TabIndex = 40;
            this.label21.Text = "PhoneNo:";
            // 
            // txt_phone
            // 
            this.txt_phone.Location = new System.Drawing.Point(364, 126);
            this.txt_phone.Name = "txt_phone";
            this.txt_phone.ReadOnly = true;
            this.txt_phone.Size = new System.Drawing.Size(100, 22);
            this.txt_phone.TabIndex = 41;
            // 
            // homepage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(891, 616);
            this.Controls.Add(this.panel_detail);
            this.Controls.Add(this.btn_showall);
            this.Controls.Add(this.btn_add);
            this.Controls.Add(this.btn_search);
            this.Controls.Add(this.txt_search);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "homepage";
            this.Text = "Asset Manager";
            this.panel_detail.ResumeLayout(false);
            this.panel_detail.PerformLayout();
            this.panel_computer.ResumeLayout(false);
            this.panel_computer.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel_detail;
        private System.Windows.Forms.ComboBox combo_owner;
        private System.Windows.Forms.CheckBox chkbox_use;
        private System.Windows.Forms.Button btn_delete;
        private System.Windows.Forms.Button btn_update;
        private System.Windows.Forms.Panel panel_computer;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txt_cpu;
        private System.Windows.Forms.TextBox txt_ram;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txt_price;
        private System.Windows.Forms.TextBox txt_location;
        private System.Windows.Forms.TextBox txt_serial;
        private System.Windows.Forms.TextBox txt_invoice;
        private System.Windows.Forms.TextBox txt_model;
        private System.Windows.Forms.TextBox txt_make;
        private System.Windows.Forms.TextBox txt_type;
        private System.Windows.Forms.TextBox txt_assetno;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_showall;
        private System.Windows.Forms.Button btn_add;
        private System.Windows.Forms.Button btn_search;
        private System.Windows.Forms.TextBox txt_search;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker date_purchase;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.CheckBox chkbox_dp;
        private System.Windows.Forms.CheckBox chkbox_vga;
        private System.Windows.Forms.CheckBox chkbox_hdmi;
        private System.Windows.Forms.CheckBox chkbox_dvi;
        private System.Windows.Forms.Button btn_cancel;
        private System.Windows.Forms.Button btn_multi;
        private System.Windows.Forms.Button btn_unlock;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txt_ipAddress;
        private System.Windows.Forms.TextBox txt_phone;
        private System.Windows.Forms.Label label21;
    }
}

